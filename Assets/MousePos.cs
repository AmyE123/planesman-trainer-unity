using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePos : MonoBehaviour
{
    public float speed = 10f;
    public Vector3 targetPos;
    public bool isMoving;
    const int MOUSE = 0;

    public Camera cam;

    void Start()
    {
        targetPos = transform.position;
        isMoving = false;
    }

    void Update()
    {
        if(Input.GetMouseButton(MOUSE))
        {
            SetTargetPos();
        }
        if(isMoving)
        {
            MoveObject();
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            ResetCamera();
        }
    }

    void SetTargetPos()
    {
        Plane plane = new Plane(Vector3.up, transform.position);
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        Debug.Log("hit: " + hit.point);

        float point = 0f;

        if(plane.Raycast(ray, out point))
            targetPos = ray.GetPoint(point);

        isMoving = true;
    }

    void MoveObject()
    {
        transform.LookAt(targetPos);
        //transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetPos.x, targetPos.y, 0), speed * Time.deltaTime);

        if(transform.position == targetPos)
            isMoving = false;

        Debug.DrawLine(transform.position, targetPos, Color.red);
    }

    void ResetCamera()
    {
        targetPos = new Vector3(0, 0, 0);
        transform.position = new Vector3(0, 0, 0);
    }
}
